#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QThread>
#include <QtQml>
#include "src/imagechange.h"
#include <QQmlContext>
#include "src/imgarrow.h"
#include "src/speedoption.h"
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<ImageChange>("imageChange", 1, 0, "ImageChange");
    qmlRegisterType<QTimer>("myTimer", 1, 0, "QTimer");
    //------------------------------
    QQmlContext* context = engine.rootContext();
    ImgArrow ia;
    context->setContextProperty("imgArrow", &ia);
    //-----Speed option define
    SpeedOption spo;
    context->setContextProperty("speedOption", &spo);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
