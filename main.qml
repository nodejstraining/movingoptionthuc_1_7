import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import imageChange 1.0
import myTimer 1.0

ApplicationWindow {
    property bool isClickArrow1: false
    property bool isClickArrow2: false
    property bool optionColVisible: false
    property bool optionDelArr: false
    visible: true
    width: 640
    height: 480
    title: qsTr("Moving")
    id: root
    function updateImage(){
        timer.start()
        changeMyImg.changedImage()
    }

    function optionChoosen(){
        //(optionMove.visible == true) ? (optionColVisible = false) : (optionColVisible = false)
        if(optionMove.visible)
            optionColVisible = false;
        else optionColVisible = true
    }

    //This is Rectangle containt Image and three Rectangle have position To move Image to
    Rectangle{

        width: root.width
        height: root.height
        id: page
        // Using QTimer to delay Image
        QTimer{
            id: timer
            interval: 200
            onTimeout: {
                updateImage()
            }
        }
        //Using it in qml to Call the function defind in cpp file
        ImageChange{
            id: changeMyImg
        }


        Image {
            id: giaodien
            source: "images/giaodienbibi.png"
            width: page.width
            height: page.height
        }
        //Image with animation
        Image{

            id: imgMove
            width: 50
            height: 100
            x: topRect.x+50;
            y: topRect.y-50;
            source: changeMyImg.sourceImage
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    optionChoosen()
                }
            }

        }
        //rectangle containt position start
        Rectangle{
            id: topRect
            width: 50;height: 50
            anchors{left: parent.left;bottom: parent.bottom ; leftMargin: 20; bottomMargin: 20}
            color: "red"; border.color: "Gray";radius: 6
            MouseArea{anchors.fill: parent; onClicked: {onStateMove()}}
        }

        //This is state move of Image people
        states:[
            State {
                name: "moveMidRight"
                PropertyChanges {
                    target: imgMove
                    x: dragImg.x
                    y: dragImg.y
                }
            },
            State {
                name: "moveMidRight2"
                PropertyChanges {
                    target: imgMove
                    x: dragImg2.x
                    y: dragImg2.y
                }
            }
        ]
        transitions: [
            Transition {
                from: "*"
                to: "moveMidRight"
                NumberAnimation{properties: "x,y"; easing.type: Easing.Linear; duration: speedOption.speed}
            },
            Transition {
                from: "*"
                to: "moveMidRight2"
                NumberAnimation{properties: "x,y"; easing.type: Easing.Linear; duration: speedOption.speed}
            },
            Transition {
                NumberAnimation{properties: "x,y"; easing.type: Easing.Linear; duration: speedOption.speed}
            }
        ]
        //------------------------------------
        //The rectangle containt the option of image
        Rectangle{

            anchors{left: parent.left; top: parent.top; leftMargin: 5; topMargin: 5}
            id: optionMove
            visible: optionColVisible
            width: 50
            height: 100
            color: "white"
            //This is option when people ischoose
            Column{
                id: optionCol
                visible: optionColVisible
                Text {
                    id: moveIcon
                    text: qsTr("Option")
                }
                //option icon move
                Image {
                 //   anchors.top: moveIcon.bottom
                    id: iconArrow
                    width: 40
                    height: 30
                    source: "images/arrow.png"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            clickArrowIcon()
                           // imgArrow.newArrow()
                        }
                    }
                }
                Slider{
                    id: editSpeed
                    width: optionMove.width
                    maximumValue: 10000
                    minimumValue: 1000
                    value: 7000
                    onValueChanged: {
                        speedOption.changeSpeedOption(editSpeed.value)
                    }
                }
            }

        }
        //option delete icon arrow
//        Rectangle{
//            id: rectOptionDelArr
//            width: page.width/20
//            height: page.height/13
//            visible: optionDelArr
//            Image {
//                width: rectOptionDelArr.width
//                height: rectOptionDelArr.height
//                id: imgDel
//                source: "images/del.jpg"
//            }
//        }

        //visible arrow in main screen
        Rectangle{
            id: rectDragImg
            x: optionMove.x+55
            y: 20

            Image {
                visible: isClickArrow1
                width: iconArrow.width
                height: iconArrow.height
                id: dragImg
                source: "images/arrow.png"
                Drag.active: dragImgArea.drag.active
                Drag.hotSpot.x: dragImg.x
                Drag.hotSpot.y: dragImg.y
                MouseArea{
                    id: dragImgArea
                    anchors.fill: parent
                    drag.target: parent
                    onClicked: {
//                        updateImage()
//                        page.state ="moveMidRight"
                        optionHideArrow()
                    }
                }
            }
        }
        //Rectangle have image icon delete arrow cursor
        Rectangle{
            id: rectDragImg2
            x: optionMove.x+55
            y: 20

            Image {
                visible: isClickArrow2
                width: iconArrow.width
                height: iconArrow.height
                id: dragImg2
                source: "images/arrow.png";
                Drag.active: dragImgArea2.drag.active
                Drag.hotSpot.x: dragImg2.x
                Drag.hotSpot.y: dragImg2.y
                MouseArea{
                    id: dragImgArea2
                    anchors.fill: parent
                    drag.target: parent
                    onClicked: {
//                        updateImage()
//                        page.state ="moveMidRight"
                        optionHideArrow()
                    }
                }
            }
        }
    }
    //
    function clickArrowIcon(){
        if(!dragImg.visible && !dragImg2.visible){
            isClickArrow1 = true
        }else if(dragImg.visible && !dragImg2.visible){
            isClickArrow2 = true
        }else if(!dragImg.visible && dragImg2.visible){
            isClickArrow1 = true
        }
    }
    //
    function optionHideArrow(){
        //isClickArrow1 = false
    }
    function onStateMove(){
        updateImage();
        page.state="moveMidRight"
        if(imgMove.x == dragImg.x)
            page.state="moveMidRight2"
    }
}
