#ifndef IMGARROW_H
#define IMGARROW_H
#include <QObject>
class ImgArrow : public  QObject
{
    Q_OBJECT
    Q_PROPERTY(int imageArrow READ imageArrow WRITE setImageArrow NOTIFY imageArrowChanged)
public:
    explicit ImgArrow(QObject* parent = 0);
    int imageArrow(){return imgArrow;}
    void setImageArrow(int str_arr);
signals:
    void imageArrowChanged();
public slots:
    void newArrow();
private:
    int number;
    int imgArrow;
};
#endif // IMGARROW_H
