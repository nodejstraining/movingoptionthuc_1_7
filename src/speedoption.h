#ifndef SPEEDOPTION_H
#define SPEEDOPTION_H
#include <QObject>
class SpeedOption : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int speed READ speed WRITE setSpeed NOTIFY speedChanged)
public:
    explicit SpeedOption(QObject* parent = 0);
    int speed(){return speedOption;}
    void setSpeed(int spd);
signals:
    void speedChanged();
public slots:
    void changeSpeedOption(int mySpd);
private:
    int speedOption;
};
#endif // SPEEDOPTION_H
