#include "speedoption.h"
#include <QObject>

SpeedOption::SpeedOption(QObject *parent): QObject(parent){
    speedOption = 7000;
}

void SpeedOption::setSpeed(int spd){
    speedOption = spd;
    emit speedChanged();
}

void SpeedOption::changeSpeedOption(int mySpd){
    setSpeed(mySpd);
}
